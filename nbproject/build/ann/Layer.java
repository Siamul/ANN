/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ann;

import org.jblas.DoubleMatrix;
import java.util.Random;

/**
 *
 * @author Siamul Karim Khan
 */
public class Layer {
    DoubleMatrix W;
    DoubleMatrix x;
    DoubleMatrix Wtx;
    double[] softmax;
    double[] nsoftmax;
    double[] bias;
    public void setUpLayer(int layerSize, int prevLayerSize)
    {
        double[][] W = new double[layerSize][prevLayerSize];
        double[][] x = new double[prevLayerSize][1];
        bias = new double[layerSize];
        softmax = new double[layerSize];
        nsoftmax = new double[layerSize];
        //this.W = DoubleMatrix.rand(layerSize, prevLayerSize);
        Random r = new Random();
        for(int j = 0; j<W.length; j++)
        {
            for(int i = 0; i<W[0].length; i++)
            {
                W[j][i] = r.nextDouble();
            }
            bias[j] = r.nextDouble();
        }
        this.W = new DoubleMatrix(W);
    }
    public void update(boolean isSoftmax)
    {
        Wtx = W.mmul(x);
        for(int i = 0; i<bias.length; i++)
        {
            Wtx.data[i] += bias[i];
        }
        if(isSoftmax)
        {
            double max = -999999;
            for(int i = 0; i<Wtx.rows; i++)
            {
                if(max < Wtx.get(i, 0))
                {
                    max = Wtx.get(i, 0);
                }
            }
            for(int i = 0; i<Wtx.rows; i++)
            {
                softmax[i] = Math.exp(Wtx.get(i,0) - max);
                //System.out.println("look at me = "+Wtx.get(i,0));
            }
            double sum = 0;
            for(int i = 0; i<softmax.length; i++)
            {
                sum += softmax[i];
            }
            for(int i = 0; i<nsoftmax.length; i++)
            {
                nsoftmax[i] = softmax[i]/sum;
             //   System.out.println("soft = "+softmax[i]+" and sum = "+sum);
            }
        }
        else
        {
            for(int i = 0; i<Wtx.rows; i++)
            {
                nsoftmax[i] = NeuralNetwork.f(Wtx.get(i,0));
            }
        }
    }
    public double[] getSum()
    {
        double[] retV = new double[Wtx.rows];
        for(int i = 0; i<Wtx.rows; i++)
        {
            retV[i] = Wtx.get(i, 0);
        }
        return retV;
    }
    public double getVal(int nodeNo)
    {
        return nsoftmax[nodeNo];
    }
    public double[] getY()
    {
        double[] retV = new double[nsoftmax.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = nsoftmax[i];
        }
        return retV;
    }
    public DoubleMatrix getW()
    {
        return W;
    }
//    public void setWeight(double[] weight, int row)
//    {
//        double[][] arr = W.getArrayCopy();
//        for(int i = 0; i<arr[row].length; i++)
//        {
//            arr[row][i] = weight[i];
//        }
//        W = new Matrix(arr); 
//    }
    public void addWeight(double[] dweight, int row)
    {
        
        for(int i = 0; i<W.columns; i++)
        {
            W.data[i*W.rows + row] += dweight[i];
        }
    }
    public double[] getWeight(int row)
    {
        double[] retV = new double[W.columns];
        for(int i = 0; i<W.columns; i++)
        {
            retV[i] = W.get(row, i);
        }
        return retV;
    }
    public void setx(double[] x)
    {
        double[][] arg = new double[x.length][1];
        for(int i = 0; i<x.length; i++)
        {
            arg[i][0] = x[i];
        }
        this.x = new DoubleMatrix(arg);
    }
}

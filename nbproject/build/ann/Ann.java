/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ann;
import org.jblas.DoubleMatrix;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
/**
 *
 * @author Siamul Karim Khan
 */
public class Ann{

    /**
     * @param args the command line arguments
     */
    
    static int layerNo = 0;
    static int[] nodeNo;
    static int dimension1 = 28;//28
    static int dimension2 = 28;//28
    static int output_node = 10;
    
    static int[] label;
    static int[] label2;
    static double[][] datasetarr;
    static double[][] test_datasetarr;
    
   
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int readSize = 15000;
        int readSize2 = 0;
        String file;
        DoubleMatrix[] dataset = new DoubleMatrix[readSize];
        DoubleMatrix[] test_dataset = new DoubleMatrix[readSize2];
        for(int i = 0; i<readSize; i++)
        {
            file = "mat_data/image" + String.valueOf(i+1);
           //file = "xor" + String.valueOf(i+1);
            file += ".txt";
            BufferedReader br = new BufferedReader(new FileReader(file));
            dataset[i] = DoubleMatrix.read(br);
        }
        for(int i = 0; i<readSize2; i++)
        {
           // file = "test/test/test" + String.valueOf(i+1);
            file = "xor" + String.valueOf(i+1);
            file += ".txt";
            BufferedReader br = new BufferedReader(new FileReader(file));
            test_dataset[i] = DoubleMatrix.read(br);
        }
        label = new int[readSize];
        label2 = new int[readSize2];
        try(BufferedReader br = new BufferedReader(new FileReader("mat_data/label.txt")))
        //try(BufferedReader br = new BufferedReader(new FileReader("label.txt")))
        {
            for(int i = 0; i<readSize; i++)
            {
                label[i] = Integer.valueOf(br.readLine());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        //try(BufferedReader br = new BufferedReader(new FileReader("test/label/label.txt")))
//        try(BufferedReader br = new BufferedReader(new FileReader("label.txt")))
//        {
//            for(int i = 0; i<readSize2; i++)
//            {
//                label2[i] = Integer.valueOf(br.readLine());
//            }
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//        }
//        
        //double[][] temp;
        datasetarr = new double[readSize][dimension1*dimension2];
        test_datasetarr = new double[readSize2][dimension1*dimension2];
        for(int i = 0; i<readSize; i++)
        {
            //temp = dataset[i].getArrayCopy();//get the 2D pixel matrix for each training data
            for(int j = 0; j<dimension1; j++)
            {
                for(int k = 0; k<dimension2; k++)
                {
                    datasetarr[i][j*dimension2+k] = dataset[i].get(j, k);//temp[j][k];
                }
            }
        }
        for(int i = 0; i<readSize2; i++)
        {
            //temp = dataset[i].getArrayCopy();//get the 2D pixel matrix for each training data
            for(int j = 0; j<dimension1; j++)
            {
                for(int k = 0; k<dimension2; k++)
                {
                    test_datasetarr[i][j*dimension2+k] = dataset[i].get(j, k);//temp[j][k];
                }
            }
        }
        
        
        int trainSize = 10000;//max 10k dewa jabe
        int testSize= 5000;
        
        double[][] validateDataset = new double[5000][];
        int[] validateLabel = new int[5000];
        double[][] trainDataset = new double[trainSize][];
        int[] trainLabel = new int[trainSize];
        double[][] testDataset = new double[testSize][];
        int[] testLabel = new int[testSize];
        
//        for(int i = 0; i<5000; i++)
//        {
//            validateDataset[i] = datasetarr[i];
//            validateLabel[i] = label[i];
//        }
        int valid = 0;
        for(int i = valid; i<valid+trainSize; i++)
        {
            trainDataset[i-valid] = datasetarr[i];
            trainLabel[i-valid] = label[i];
        }
        
        for(int i = trainSize; i<trainSize+testSize; i++)
        {
            testDataset[i-trainSize] = datasetarr[i];
            testLabel[i-trainSize] = label[i];
        }
        
        
//        prepare(0);
//        double max = 99999;
//        int l=0,n=0;
//        for(int i=2;i<5;i++)//layer no
//        {
//            for(int j=5; j<100; j++)
//            {
//                nodeNo = new int[i];//dummy
//                Arrays.fill(nodeNo, j);
//                nodeNo[nodeNo.length - 1] = output_node;
//                
//                NeuralNetwork nw = new NeuralNetwork(i, nodeNo);
//                nw.setData(validateDataset, label);
//                nw.backPropagation(); 
//                
//                double err = nw.errorRate;
//                if(max > err)
//                {
//                    max = err;
//                    l = i;
//                    n = j;
//                }
//                System.out.println("Completed iteration for layerNo = "+i+",nodeNo = "+j+",ERROR RATE = "+err);
//            }
//        }
        //System.out.println("Validation Complete");
        //System.out.println("Chosen value of layerNo = "+l+", nodeNo = "+n);
        
        int l = 2;
        int n = 15;
        //prepare(1);
        System.out.println("data prepared 1");
        nodeNo = new int[l];//dummy
        Arrays.fill(nodeNo,n);
        nodeNo[nodeNo.length - 1] = output_node;
        
        NeuralNetwork nw = new NeuralNetwork(l, nodeNo);
        nw.setData(trainDataset, trainLabel);
        nw.backPropagation(); 
        
        System.out.println("Training Complete");
        
        //prepare(2);
        System.out.println("data prepared 2");
        double correct = 0;
        for(int i=0;i<testDataset.length;i++)
        {
            //System.out.println("Test data-"+i);
            
            int count = 0;
            double[] output = new double[output_node];
            int[] predicted_output = new int[output_node];
            
            output = nw.getOutput(testDataset[i]);
            
            int true_val = testLabel[i];
            //System.out.println("real value = "+true_val);
            double outputMax = -999999;
            int maxIndex = -1;
            for(int j = 0;j<output.length;j++)
            {
                if(outputMax < output[j])
                {
                    outputMax = output[j];
                    maxIndex = j;
                }
             //System.out.println("output node "+j+", value = "+output[j]);
            }
            if(maxIndex == true_val)
            {
                correct++;
                //System.out.println("Yes it is correctly predicted");
            }
        }
        System.out.println("Accuracy = "+(correct*100)/(double)testDataset.length+"%");
        
    }   
}

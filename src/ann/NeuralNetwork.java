/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ann;

import java.util.ArrayList;
import java.util.Collections;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Siamul Karim Khan
 */
public class NeuralNetwork {
    Layer[] layers;
    double[][] dataset;
    double[][] vdataset;
    int[] label;
    int[] vlabel;
    int layerNo;
    int[] nodeNo;
    //double learningRate = 0.0001;
    double learningRate = 4;
    static double a = 0.001;
    //static double a = 1;
    boolean isSoftmax = false;
    boolean adaptive = true;
    boolean finalLayer = false;
    
    public NeuralNetwork(int layerNo, int[] nodeNo)
    {
        this.layerNo = layerNo;
        this.nodeNo = nodeNo;
        layers = new Layer[layerNo];
        for(int i = 1; i<layerNo; i++)
        {
//            System.out.println("sumit");
            layers[i] = new Layer();
            layers[i].setUpLayer(nodeNo[i], nodeNo[i-1]);
        }
        layers[0] = new Layer();
        layers[0].setUpLayer(nodeNo[0],ann.dimension1*ann.dimension2);
    }
    public double[] getOutput(double[] x)
    {
        layers[0].setx(x);
        for(int i = 0; i<layers.length - 1; i++)
        {
            layers[i].update(isSoftmax);
            layers[i+1].setx(layers[i].nsoftmax);
        }
        layers[layers.length - 1].update(isSoftmax || finalLayer);
        return layers[layers.length - 1].nsoftmax;
    }
    public void setData(double[][] dataset, int[] label)
    {
        int divide = 8;
        int h = dataset.length/divide;
        this.vlabel = new int[h];
        this.vdataset = new double[h][dataset[0].length];
        this.label = new int[divide*h];
        this.dataset = new double[divide*h][dataset[0].length];
         ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=0; i<dataset.length; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for(int i = 0; i<h; i++)
        {
            this.vdataset[i] = dataset[list.get(i)];
            this.vlabel[i] = label[list.get(i)];
        }
        for(int i = h; i<dataset.length; i++)
        {
            this.dataset[i-h] = dataset[list.get(i)];
            this.label[i-h] = label[list.get(i)];
        }
    }
   
    public void backPropagation()
    {
        double limit = 0.05;
        double errorRate;
        double prevErrorRate = 1;
        int count = 0;
        double accuracy;        
        while(true){
            //Matrix[] prevW = new Matrix[layers.length];
            //double[][] prevBias = new double[layers.length][];
//            DoubleMatrix[] wCopy = new DoubleMatrix[layers.length];
//            for(int r = 0; r<layers.length; r++)
//            {
//                wCopy[r] = layers[r].W.add(DoubleMatrix.zeros(layers[r].W.rows, layers[r].W.columns));
//            }
            double[][] delL = new double[dataset.length][nodeNo[layerNo-1]];
            double[][][] sumValues = new double[dataset.length][layerNo][];
            double[][][] input = new double[dataset.length][layerNo+1][];
            double[][] error = new double[dataset.length][nodeNo[layerNo - 1]];
            //double entropyError = 0;
            for(int i = 0; i<dataset.length; i++)
            {
                input[i][0] = dataset[i];
                input[i][layerNo] = getOutput(dataset[i]);
                sumValues[i][layerNo - 1] = layers[layerNo - 1].getSum();
                for(int k = 0; k<layerNo - 1; k++)
                {
                    sumValues[i][k] = layers[k].getSum();
                    input[i][k+1] = layers[k].getY();
                }
                double[] trueOutput = new double[nodeNo[layerNo - 1]];
                trueOutput[label[i]] = 1.0;
                for(int j = 0; j<nodeNo[layerNo - 1]; j++)
                {
                    error[i][j] = (input[i][layerNo][j] - trueOutput[j]);
                    if(isSoftmax || finalLayer)
                    {
                        delL[i][j] = error[i][j];
                       // entropyError += trueOutput[j]*ln(input[i][layerNo][j]) + (1 - trueOutput[j])*ln(1 - input[i][layerNo][j]);
                    }
                    else delL[i][j] = error[i][j]*(fprime(sumValues[i][layerNo - 1][j])); 
                }
            }
            double[] errorSum = new double[dataset.length];
            errorRate = 0;
            for(int i = 0; i<dataset.length; i++)
            {
                for(int j = 0; j<nodeNo[layerNo - 1]; j++)
                {
                    errorSum[i] += Math.pow(error[i][j], 2); 
                }
                errorSum[i] /= nodeNo[layerNo - 1];
                errorSum[i] = Math.sqrt(errorSum[i]);
                errorRate += errorSum[i];
            }
            errorRate /= dataset.length;
            //errorRate = Math.sqrt(errorRate);
            if(adaptive){
                if(errorRate - prevErrorRate <= 0) learningRate *= 1.05;
                if(errorRate - prevErrorRate > 0) learningRate /= 4;
                prevErrorRate = errorRate;
            }
            double correct = 0;
            for(int i=0;i<vdataset.length;i++)
            {
                double[] output = new double[ann.output_node];
                int[] predicted_output = new int[ann.output_node];

                output = getOutput(vdataset[i]);

                int true_val = vlabel[i];
                double outputMax = -999999;
                int maxIndex = -1;
                for(int j = 0;j<output.length;j++)
                {
                    if(outputMax < output[j])
                    {
                        outputMax = output[j];
                        maxIndex = j;
                    }
                }
                if(maxIndex == true_val)
                {
                    correct++;
                }
            }
            accuracy = (correct*100)/(double)vdataset.length;
            //System.out.println(accuracy);
            count++;
            System.out.println("count = "+count+", accuracy = "+accuracy+", errorRate = " + errorRate);
            if(accuracy >= 85) break;
            if(errorRate < limit) break;
//            if(isSoftmax)
//            {
//                for(int i = 0; i<dataset.length; i++)
//                {
//                    for(int j = 0; j<nodeNo[layerNo - 1]; j++)
//                    {
//                        DoubleMatrix outMat = new DoubleMatrix(input[i][layerNo]);
//                        DoubleMatrix errMat = new DoubleMatrix(error[i]);
//                        DoubleMatrix Msum = outMat.transpose().mmul(errMat);
//                        delL[i][j] = (error[i][j] - Msum.data[0])*input[i][layerNo][j];
//                    }
//                }
//            }
            double[][][] del = new double[dataset.length][layerNo][];
            for(int i = 0; i<dataset.length; i++)
            {
                del[i][layerNo - 1] = new double[nodeNo[layerNo - 1]];
                for(int j = 0; j<nodeNo[layerNo - 1]; j++)
                {
                    del[i][layerNo - 1][j] = delL[i][j];
                }
            }
            for(int i = 0; i<dataset.length; i++)
            {
                for(int r = layerNo - 1; r>0; r--)
                {
                    del[i][r-1] = new double[nodeNo[r-1]];
                    for(int j = 0; j<nodeNo[r-1]; j++)
                    {
                        double sum = 0;
                        for(int k = 0; k<nodeNo[r]; k++)
                        {
                            if(isSoftmax) sum += (layers[r].getWeight(k)[j] - sumValues[i][r][k])*input[i][r][k]*(del[i][r][k]);
                            else sum += (layers[r].getWeight(k)[j])*(del[i][r][k]);
                        }
                        if(isSoftmax) del[i][r-1][j] = sum;
                        else del[i][r-1][j] = sum*fprime(sumValues[i][r-1][j]);
                    }
                }
            }
            double[][][] delW = new double[layerNo][][];
            double[][] delBias = new double[layerNo][];
            for(int r = 0; r<layerNo; r++)
            {
                delW[r] = new double[nodeNo[r]][];
                delBias[r] = new double[nodeNo[r]];
                for(int j = 0; j<nodeNo[r]; j++)
                {
                    double[] sum = new double[input[0][r].length];
                    double biasChange = 0;
                    for(int i = 0; i<dataset.length; i++)
                    {
                            sum = arraySum(sum, arrayMultiply(input[i][r], del[i][r][j]));
                            biasChange += del[i][r][j];
                    }
                    delW[r][j] = arrayMultiply(sum, -learningRate);
                    delBias[r][j] = -learningRate*biasChange;
                }
            }
            for(int r = 0; r<layerNo; r++)
            {
                for(int j = 0; j<nodeNo[r]; j++)
                {
                    layers[r].addWeight(delW[r][j], j);
                    layers[r].bias[j] += delBias[r][j];
                    
                }
            }          
        }
        System.out.println("count = "+count);
    }
    public static double f(double val)
    {
        double value = 1.0/(1.0+(Math.exp(-a*val)));
        return value;
        //return val+0.5;
    }
    public static double softmaxf(double[] sums, int index)
    {
        double totalExpSum = 0;
        double expSumIndex = 0;
        double max = -999999;
        for(int i = 0; i<sums.length; i++)
        {
            if(max < sums[i])
            {
                max = sums[i];
            }
        }
        for(int i = 0; i<sums.length; i++)
        {
            if(i == index) expSumIndex = Math.exp(sums[i] - max);
            totalExpSum += Math.exp(sums[i] - max);
        }
        return expSumIndex/totalExpSum;
    }
    public static double softmaxfprime(double[] sums, int nindex, int dindex)
    {
        if(nindex == dindex)
        {
            double val = softmaxf(sums, nindex);
            return val*(1-val);
        }
        else
        {
            return -1*softmaxf(sums, nindex)*softmaxf(sums, dindex);
        }
    }
    public static double fprime(double value)
    {    
        double val = a*f(value)*(1-f(value));
        return val;
    }
    public static void printArr(double[] arr)
    {
        for(int i = 0; i<arr.length; i++)
        {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    public double ln(double val)
    {
        return Math.log(val);
    }
    public double[] arrayMultiply(double[] arr, double val)
    {
        double[] retV = new double[arr.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = val * arr[i];
        }
        return retV;
    }
    public double[] arraySum(double[] arr1, double[] arr2)
    {
        double[] retV = new double[arr1.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = arr1[i] + arr2[i];
        }
        return retV;
    }
}
